from django.db import models
from customauth.models import User


# Create your models here.
class Images(models.Model):
    u_id = models.ForeignKey(User, on_delete=models.CASCADE)
    url = models.URLField()


class Places(models.Model):
    name = models.CharField(max_length=200)
    loc = models.CharField(max_length=200)
    category = models.CharField(max_length=50)
    description = models.TextField()
    image = models.URLField()
    rating = models.IntegerField()


class Visitors(models.Model):
    p_id = models.ForeignKey(Places, on_delete=models.CASCADE)
    u_id = models.ForeignKey(User, on_delete=models.CASCADE)
    timestamp = models.DateTimeField()


class Bibliography(models.Model):
    u_id = models.ForeignKey(User, on_delete=models.CASCADE)
    content = models.TextField()


class Scams(models.Model):
    name = models.CharField(max_length=500)
    my_experiance = models.TextField()
    how_to_avoid_it = models.TextField()


class FirstAid(models.Model):
    name = models.CharField(max_length=500)
    symptom = models.TextField()
    need = models.TextField()
    treatment = models.TextField()


class Insurance(models.Model):
    name = models.CharField(max_length=500)
    validity = models.TextField()
    company = models.TextField()
    cost = models.IntegerField()
    covered = models.TextField()


class ChatForum(models.Model):
    u_id = models.ForeignKey(User, on_delete=models.CASCADE)
    message = models.TextField()
    timestamp = models.DateTimeField()