import urllib
from io import BytesIO
import datetime as d
from django.shortcuts import render
from django.http import JsonResponse, HttpResponse
from django.core import serializers
from .chatbot import *
from .objectdec import *
import tensorflow as tf
from django.views.decorators.csrf import csrf_exempt
from .models import ChatForum, User, Images, Visitors, Places
from .serializers import ChatForumSerializer, VisitorsSerializer

graph = tf.get_default_graph()


@csrf_exempt
def get_bot_response(request):
    if request.method == "POST":
        userText = request.POST['msg']
        response = {
            'res': str(english_bot.get_response(userText))
        }
        # data = serializers.serialize("json", response)
        return JsonResponse(response)


# return data

@csrf_exempt
def get_image_label(request):
    if request.method == "GET":
        url = request.GET['url']
        u_id = request.GET['uid']

        i = Images()
        i.url = url
        i.u_id = User.objects.get(id=2)
        i.save()

        with urllib.request.urlopen(url) as url:
            img = image.load_img(BytesIO(url.read()), target_size=(28, 28, 3), grayscale=False)

        img = image.img_to_array(img)

        img = img / 255
        l = [img]
        a = np.array(l)
        objs = {0: 'Basilica of Bom Jesus', 1: 'Lady of Immaculate Conception Goa'}
        with graph.as_default():
            prob = (model.predict_proba(a))
            obj = objs[model.predict_classes(a)[0]]

        # return JsonResponse({
        #     'obj':prob[0][0]
        # })

        if prob[0][0] > 0.6 or prob[0][1] > 0.6:
            p = Places.objects.get(name=obj)
            v = Visitors()
            v.p_id = p
            v.u_id = User.objects.get(id=2)
            v.timestamp = d.datetime.now()
            v.save()
            response = {
                'probability': str(prob),
                'object': obj,
                'desc': p.description,
                'loc': p.loc,
                'msg': 'success'
            }

        else:
            response = {
                'msg': 'error',
                'description': 'could not detect object',
                'prob': str(prob)
            }

        return JsonResponse(response)


@csrf_exempt
def get_chatforum_data(request):
    if request.method == "POST":
        # data = serializers.serialize("json", ChatForum.objects.all())
        data2 = ChatForum.objects.all()
        data1 = ChatForumSerializer(data2, many=True)

        return JsonResponse(data1.data, safe=False)


@csrf_exempt
def add_chatforum_data(request):
    if request.method == "POST":
        u_id = request.POST['u_id']
        # user = User.objects.get(id=u_id)
        content = request.POST['msg']
        # timestamp = d.datetime.now()

        chat = ChatForum()

        chat.u_id = User.objects.get(id=u_id)
        chat.message = content
        chat.timestamp = d.datetime.now()

        chat.save()

        data2 = ChatForum.objects.all()
        data1 = ChatForumSerializer(data2, many=True)

        return JsonResponse(data1.data, safe=False)


def index(request):
    return render(request, "VTA.html")


@csrf_exempt
def get_visitors_data(request):
    if request.method == "GET":
        id = request.GET['id']
        data1 = Visitors.objects.filter(p_id=id)
        data2 = VisitorsSerializer(data1, many=True)

        return JsonResponse(data2.data, safe=False)
