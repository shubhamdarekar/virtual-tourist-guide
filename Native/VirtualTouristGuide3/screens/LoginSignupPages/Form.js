import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {Dimensions} from 'react-native';

import {
  StyleSheet,
  KeyboardAvoidingView,
  View,
  ActivityIndicator,
  TouchableOpacity,
  Image,
} from 'react-native';

import UserInput from './UserInput';

import usernameImg from '../../assets/images/username.png';
import passwordImg from '../../assets/images/password.png';
import eyeImg from '../../assets/images/eye_black.png';

export default class Form extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showPass: true,
      press: false,
      uname:'',
      pass:''
    };
    this.showPass = this.showPass.bind(this);
    this.callBackFunction1 = this.callBackFunction1.bind(this);
    this.callBackFunction2 = this.callBackFunction2.bind(this);
  }

  callBackFunction1(uname){
      this.setState({uname});
      this.props.uname(uname);
  }

  callBackFunction2(pass){
    this.setState({pass});
    this.props.pass(pass);
  }

  showPass() {
    this.state.press === false
      ? this.setState({showPass: false, press: true})
      : this.setState({showPass: true, press: false});
  }

  render() {
    return (
      <KeyboardAvoidingView behavior="padding" style={styles.container}>
        <UserInput
          source={usernameImg}
          placeholder="Mobile Number"
          autoCapitalize={'none'}
          returnKeyType={'done'}
          autoCorrect={false}
          keyboardType= {'phone-pad'}
          parentCall = {this.callBackFunction1}
          autoFocus = {true}
        />
        <UserInput
          source={passwordImg}
          secureTextEntry={this.state.showPass}
          placeholder="Password"
          returnKeyType={'done'}
          autoCapitalize={'none'}
          autoCorrect={false}
          parentCall = {this.callBackFunction2}
        />
        <TouchableOpacity
          activeOpacity={0.7}
          style={styles.btnEye}
          onPress={this.showPass}>
          <Image source={eyeImg} style={styles.iconEye} />
        </TouchableOpacity>
        
      </KeyboardAvoidingView>
    );
  }
}

const DEVICE_WIDTH = Dimensions.get('window').width;
const DEVICE_HEIGHT = Dimensions.get('window').height;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
  },
  btnEye: {
    position: 'absolute',
    top: 74,
    right: 28,
  },
  iconEye: {
    width: 25,
    height: 25,
    tintColor: 'rgba(0,0,0,0.2)',
  },
});
