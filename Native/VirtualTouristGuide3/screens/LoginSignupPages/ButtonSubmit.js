import React, { Component } from 'react';
import { Dimensions, AsyncStorage, ToastAndroid, Vibration } from 'react-native';

import {
  StyleSheet,
  TouchableOpacity,
  Text,
  Animated,
  Easing,
  Image,
  View,
} from 'react-native';


import spinner from '../../assets/images/loading.gif';
import { serverUrl } from '../../constants/url'

const DEVICE_WIDTH = Dimensions.get('window').width;
const DEVICE_HEIGHT = Dimensions.get('window').height;
const MARGIN = 40;

export default class ButtonSubmit extends Component {
  constructor() {
    super();

    this.state = {
      isLoading: false,
    };

    this.buttonAnimated = new Animated.Value(0);
    this.growAnimated = new Animated.Value(0);
    this._onPress = this._onPress.bind(this);
  }

  _onPress() {
    if (this.state.isLoading) return;
    Vibration.vibrate(30);
    this.setState({ isLoading: true });
    Animated.timing(this.buttonAnimated, {
      toValue: 1,
      duration: 200,
      easing: Easing.linear,
    }).start();

    console.log(this.props.uname);
    console.log(this.props.pass);
    fetch(serverUrl.toString() + '/login/', {
      method: 'POST',
      headers: new Headers({
        'Content-Type': 'application/x-www-form-urlencoded',
      }),
      body: 'mobile=' + this.props.uname + '&pwd=' + this.props.pass,
    }).then((response) => response.json())
      .then((responseJson) => {

        console.log(responseJson);
        if (responseJson.msg === "success") {
          setTimeout(() => {
            this._onGrow();
          }, 2000);
          setTimeout(() => {
            this.setState({ isLoading: false });
            this.buttonAnimated.setValue(0);
            this.growAnimated.setValue(0);
          }, 2300);
          AsyncStorage.setItem('isLoggedIn', 'true');
          AsyncStorage.setItem('userid', responseJson.session_id);
          this.props.pressed();
        }
        else if (responseJson.msg === 'error') {
          Vibration.vibrate(150);
          setTimeout(() => {
            this._onGrow();
          }, 300);
          setTimeout(() => {
            this.setState({ isLoading: false });
            this.buttonAnimated.setValue(0);
            this.growAnimated.setValue(0);
            ToastAndroid.show('Wrong Credntials', ToastAndroid.SHORT);
          }, 700);
        }

      }).catch((error) => {
        Vibration.vibrate(150);
        console.log(error + "Server Error or No Internet Connection");
        setTimeout(() => {
          this._onGrow();
        }, 300);
        setTimeout(() => {
          this.setState({ isLoading: false });
          this.buttonAnimated.setValue(0);
          this.growAnimated.setValue(0);
          ToastAndroid.show('Server Error or No Internet Connection', ToastAndroid.SHORT);
        }, 700);
      });



  }

  _onGrow() {
    Animated.timing(this.growAnimated, {
      toValue: 1,
      duration: 200,
      easing: Easing.linear,
    }).start();
  }

  render() {
    const changeWidth = this.buttonAnimated.interpolate({
      inputRange: [0, 1],
      outputRange: [DEVICE_WIDTH - MARGIN, MARGIN],
    });
    const changeScale = this.growAnimated.interpolate({
      inputRange: [0, 1],
      outputRange: [1, MARGIN],
    });

    return (
      <View style={styles.container}>
        <Animated.View style={{ width: changeWidth }}>
          <TouchableOpacity
            style={styles.button}
            onPress={this._onPress}
            activeOpacity={1}>
            {this.state.isLoading ? (
              <Image source={spinner} style={styles.image} />
            ) : (
                <Text style={styles.text}>LOGIN</Text>
              )}
          </TouchableOpacity>
          <Animated.View
            style={[styles.circle, { transform: [{ scale: changeScale }] }]}
          />
        </Animated.View>
      </View>

    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    top: -130,
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  button: {
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#F035E0',
    height: MARGIN,
    borderRadius: 20,
    zIndex: 100,
  },
  circle: {
    height: MARGIN,
    width: MARGIN,
    marginTop: -MARGIN,
    borderWidth: 1,
    borderColor: '#F035E0',
    borderRadius: 100,
    alignSelf: 'center',
    zIndex: 99,
    backgroundColor: '#F035E0',
  },
  text: {
    color: 'white',
    backgroundColor: 'transparent',
  },
  image: {
    width: 24,
    height: 24,
  },
});

