import React, {Component} from 'react';
import { StyleSheet, View,KeyboardAvoidingView} from 'react-native';
import Logo from './Logo';
import Form from './Form';
import Wallpaper from './Wallpaper';
import ButtonSubmit from './ButtonSubmit';
import SignupSection from './SignupSection';
import { NavigationActions ,StackActions} from 'react-navigation';


export default class LoginView extends Component {
    constructor(props) {
        super(props);
        this.state = {
          uname   : '',
          pass: '',
        };      
    this.callBack1 = this.callBack1.bind(this);
    this.callBack2 = this.callBack2.bind(this);
    this.pressed = this.pressed.bind(this);
    }

    
    

    

    callBack1(uname){
      this.setState({uname});
    }
    callBack2(pass){
      this.setState({pass});
    }
    pressed(){
      this.props.navigation.navigate('loggedin');
    }

  render() {
    return (
      <Wallpaper style = {{flex:1,flexDirection: 'column',
      justifyContent: 'space-around',}}>
        <Logo />
        <Form
        uname = {this.callBack1}
        pass = {this.callBack2}
        />
        <SignupSection/>
        <ButtonSubmit
        uname = {this.state.uname}
        pass = {this.state.pass}
        pressed = {this.pressed}
        />
      </Wallpaper>
    );
  }
}

