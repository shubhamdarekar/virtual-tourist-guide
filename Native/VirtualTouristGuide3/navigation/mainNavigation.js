import { createAppContainer, createSwitchNavigator } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import { AsyncStorage } from 'react-native';
import React,{Component} from 'react';



import LoginView from '../screens/LoginPage';
import IntroSlider from '../screens/introSlider';
import LoginView2 from '../screens/LoginSignupPages/LoginScreen'


// const rfun = (init) =>{
//    return RootStack = createStackNavigator( {
//     Intro: IntroSlider,
//     LoginPage: LoginView,
//     Home: AfterLoginView
//   },
//   {
//     defaultNavigationOptions: {
//       headerMode: 'none',
//       headerVisible: false,
//       header: null,
//     },
//     navigationOptions: {
//       headerMode: 'none',
//       headerVisible: false,
//       header: null,
//     },
//     initialRouteName:init,
//   } );
  

// }






// // export default AppContainer;


// export default class NotLoggedinContainer extends Component{
//   constructor(props){
//     super(props);
//     this.state = {
//       nft : 'Intro',
//     };
//   }

//   componentDidMount() {
//     AsyncStorage.getItem('not_first_time').then((value) => {
//       if(value == 'true'){
//         this.setState({ nft:'LoginPage' });
//       }
//       else{
//         this.setState({ nft:'Intro' });
//       }
//     });
//   }

  
//   render(){
//     const AppContainer =  createAppContainer(rfun(this.state.nft));
//     return <AppContainer/>
//   }

// }

//for not allo=wing back
// this.props.navigation.dispatch(StackActions.reset({
//   index: 0,
//   key: null,
//   actions: [NavigationActions.navigate({ routeName: 'Login Page' })]
// }))

//for allowing back
//this.props.navigation.navigate('LoginPage')

export const AuthContainer =(initroute) =>  createAppContainer(
  createSwitchNavigator(
    {
      nloggedin: LoginView,
      loggedin:LoginView2,
    },
    {
      initialRouteName: initroute,
    }
  )
);

export const FTimeContainer =(initroute, loggedin) => createAppContainer(
  createSwitchNavigator(
    {
      ftime : IntroSlider,
      nftime : AuthContainer(loggedin),
    },
    {
      initialRouteName: initroute,
    }
  )
);