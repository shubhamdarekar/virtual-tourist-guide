import React from 'react';
import MapView from 'react-native-maps';
import { Marker } from 'react-native-maps';
import { StyleSheet, Text, View, Dimensions,Button } from 'react-native';
import { Component } from 'react';
import { Platform } from 'react-native';
import Constants from 'expo-constants';
import * as Location from 'expo-location';
import * as Permissions from 'expo-permissions';
import { FontAwesome } from '@expo/vector-icons';
import { MaterialIcons } from '@expo/vector-icons';


export default class App extends Component {
  constructor(props) {
  super(props);
  this.state = {
    location: null,
    errorMessage: null,
    address: null,
    markers :[],
    
    garage_markers: [
        {
          title: 'Calangute Beach',
          coordinates: {
            latitude: 15.533414,
            longitude: 73.764954
          }
        },
        {
          title: 'Baga Beach',
          coordinates: {
            latitude: 15.5553,
            longitude: 73.7517
          }
        },
        {
          title: 'Palolem Beach',
          coordinates: {
            latitude: 15.01,
            longitude: 74.0232
          }
        },
        {
          title: 'Anjuna Beach',
          coordinates: {
            latitude: 15.5736,
            longitude: 73.7407
          }
        }
      ],
    
    police_markers: [
      {
        title: 'basilica of bom jesus',
        coordinates: {
          latitude: 15.5009,
          longitude: 73.9116
        }
      },
      {
        title: 'old lady immaculate conception',
        coordinates: {
          latitude: 15.498333,
          longitude: 73.829444
        }
      },
      {
        title: 'Se Cathedral',
        coordinates: {
          latitude: 15.5039,
          longitude: 73.9150
        }
      },
      {
        title: 'St. Cajetan Church',
        coordinates: {
          latitude: 15.5056,
          longitude: 73.9150
        }
      }
    ],
    // hospital_markers: [
    //   {
    //     title: 'healthway hospital',
    //     coordinates: {
    //       latitude: 15.494162,
    //       longitude: 73.831077
    //     }
    //   },
    //   {
    //     title: 'Bosio hospital',
    //     coordinates: {
    //       latitude: 15.519918,
    //       longitude: 73.770833
    //     }
    //   },
    //   {
    //     title: 'Yashraj Clinic',
    //     coordinates: {
    //       latitude: 15.551334,
    //       longitude: 73.765597
    //     }
    //   },
    //   {
    //     title: 'St. Anthonys hospital',
    //     coordinates: {
    //       latitude: 15.600436,
    //       longitude: 73.750624
    //     }
    //   }
    // ],
    // forex_markers: [
    //   {
    //     title: 'Wall street Forex',
    //     coordinates: {
    //       latitude: 15.592037,
    //       longitude: 73.808982
    //     }
    //   },
    //   {
    //     title: 'international currency exchange',
    //     coordinates: {
    //       latitude: 15.551623,
    //       longitude: 73.765312
    //     }
    //   },
    //   {
    //     title: 'gemini travels & foreign exchange',
    //     coordinates: {
    //       latitude: 15.554141,
    //       longitude: 73.757099
    //     }
    //   },
    //   {
    //     title: 'UAE exchange',
    //     coordinates: {
    //       latitude: 15.500577,
    //       longitude: 73.822989
    //     }
    //   }
    // ],
    // atm_markers: [
    //   {
    //     title: 'hdfc bank atm',
    //     coordinates: {
    //       latitude: 15.519661,
    //       longitude: 73.766975
    //     }
    //   },
    //   {
    //     title: 'axis bank atm',
    //     coordinates: {
    //       latitude: 15.535894,
    //       longitude: 73.764371
    //     }
    //   },
    //   {
    //     title: 'citi bank atm',
    //     coordinates: {
    //       latitude: 15.545437,
    //       longitude: 73.763028
    //     }
    //   },
    //   {
    //     title: 'sbi bank atm',
    //     coordinates: {
    //       latitude: 15.494532,
    //       longitude: 73.820945
    //     }
    //   }
    // ],
    // embassy_markers: [
    //   {
    //     title: 'Consulate General of Portugal',
    //     coordinates: {
    //       latitude: 15.495083,
    //       longitude: 73.827581
    //     }
    //   },
    //   {
    //     title: 'British Nationals Assistance Office – Goa',
    //     coordinates: {
    //       latitude: 15.485325,
    //       longitude: 73.809526
    //     }
    //   },
    //   {
    //     title: 'Dubai Visa',
    //     coordinates: {
    //       latitude: 15.487476,
    //       longitude: 73.824262
    //     }
    //   },
    //   {
    //     title: 'Austrian Consulate',
    //     coordinates: {
    //       latitude: 15.398748,
    //       longitude: 73.810044
    //     }
    //   }
    // ]
  };
  }
  loadMarkers = (place) => {
    if(place == "police"){
      let p = this.state.police_markers;
      this.setState({markers: p})
    }
    else if(place == "garage"){
        let p = this.state.garage_markers;
        this.setState({markers: p})
      }
    else if(place == "hospitals"){
      let p = this.state.hospital_markers;
      this.setState({markers: p})
    }
    else if(place == "forex"){
      let p = this.state.forex_markers;
      this.setState({markers: p})
    }
    else if(place == "atms"){
      let p = this.state.atm_markers;
      this.setState({markers: p})
    }
    else if(place == "embassy"){
      let p = this.state.embassy_markers;
      this.setState({markers: p})
    }
  }
  componentWillMount() {
    if (Platform.OS === 'android' && !Constants.isDevice) {
      this.setState({
        errorMessage: 'Oops, this will not work on Sketch in an Android emulator. Try it on your device!',
      });
    } else {
      this._getLocationAsync();
    }
  }

  _getLocationAsync = async () => {
    let { status } = await Permissions.askAsync(Permissions.LOCATION);
    if (status !== 'granted') {
      this.setState({
        errorMessage: 'Permission to access location was denied',
      });
    }

    let location = await Location.getCurrentPositionAsync({});
    this.setState({ location });
    let loc = {
      latitude: this.state.location.coords.latitude,
      longitude: this.state.location.coords.longitude,
    }
    let address = Location.reverseGeocodeAsync(loc);
    this.setState({ address });
  };

  render() {
    let text = 'Waiting..';
    let addr = 'Waiting..';
    if (this.state.errorMessage) {
      text = this.state.errorMessage;
    } else if (this.state.location) {
      text = JSON.stringify(this.state.location);
      addr = JSON.stringify(this.state.address);
      
    }

    return (
      <View style={styles.container}>
        <MaterialIcons.Button style={styles.button} name="drive-eta" backgroundColor="#3b5998" onPress=                     {() => this.loadMarkers("police")}>
        Church        
        </MaterialIcons.Button>  
        <MaterialIcons.Button style={styles.button} name="monetization-on" backgroundColor="#3b5998" onPress=               {() => this.loadMarkers("garage")}>
        Beach
        </MaterialIcons.Button>
        {/* <FontAwesome.Button style={styles.button} name="hospital-o" backgroundColor="#3b5998" onPress=                      {() => this.loadMarkers("hospitals")}>
        Hospitals
        </FontAwesome.Button>
        <MaterialIcons.Button style={styles.button} name="local-atm" backgroundColor="#3b5998" onPress=                     {() => this.loadMarkers("atms")}>
        ATMs
        </MaterialIcons.Button>
        <MaterialIcons.Button style={styles.button} name="account-balance" backgroundColor="#3b5998" onPress=               {() => this.loadMarkers("embassy")}>
        Embassy
        </MaterialIcons.Button>
        <MaterialIcons.Button style={styles.button} name="security" backgroundColor="#3b5998" onPress=                      {() => this.loadMarkers("police")}>
        Police Station
        </MaterialIcons.Button> */}
        <MapView
          style={styles.mapStyle}
          showsUserLocation={true}
          followsUserLocation={true}
          showsMyLocationButton={true}
        >
        {this.state.markers.map(marker => (
          <MapView.Marker 
            coordinate={marker.coordinates}
            title={marker.title}
          />
        ))}
        </MapView>
        {/* <Text style={styles.paragraph}>{addr}</Text> */}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  mapStyle: {
    width: Dimensions.get('window').width,
    height: Dimensions.get('window').height/2,
  },
  button: {
    flexDirection: "row",
    alignItems: 'center',
    width: Dimensions.get('window').width,

  },
});