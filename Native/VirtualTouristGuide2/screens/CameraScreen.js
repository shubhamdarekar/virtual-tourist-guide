// import React, { useState, useEffect } from 'react';
// import { Text, View, TouchableOpacity } from 'react-native';
// import { Camera } from 'expo-camera';
// import Swipeable from 'react-native-gesture-handler/Swipeable';


// export default function App() {
//   const [hasPermission, setHasPermission] = useState(null);
//   const [type, setType] = useState(Camera.Constants.Type.back);
//   const[flex1,setFlex1] = useState(1);
//   const[flex2,setFlex2] = useState(0);

//   useEffect(() => {
//     (async () => {
//       const { status } = await Camera.requestPermissionsAsync();
//       setHasPermission(status === 'granted');
//     })();
//   }, []);



//   if (hasPermission === null) {
//     return <View />;
//   }
//   if (hasPermission === false) {
//     return <Text>No access to camera</Text>;
//   }
//   return (
//     <View style={{ flex: flex1 }}>
      
//       <TouchableOpacity style={{ flex: 1 }}
//         onPress={() => {
//           setFlex1(0.8);
//           setFlex2( 1);
//           snap();
//         }}>
//       <Camera style={{ flex: 1 }} type={type}
//       >
      
//         <View
//           style={{
//             flex: 1,
//             backgroundColor: 'transparent',
//             flexDirection: 'row',
//           }}>
//           <TouchableOpacity
//             style={{
//               flex: 0.1,
//               alignSelf: 'flex-end',
//               alignItems: 'center',
//             }}
//             onPress={() => {
//               setType(
//                 type === Camera.Constants.Type.back
//                   ? Camera.Constants.Type.front
//                   : Camera.Constants.Type.back
//               );
//             }}>
//             <Text style={{ fontSize: 18, marginBottom: 10, color: 'white' }}> Flip </Text>
//           </TouchableOpacity>
//         </View>
        
//       </Camera>
//       </TouchableOpacity>
//       <BottomBar />
     
      
//     </View>
//   );
// }

// const BottomBar = () => {

//   return (
//     <View>
      
//     </View>
//   );
// };

import React from 'react';
import { View, Text,AsyncStorage } from 'react-native';
import { Camera } from 'expo-camera';
import {Audio } from 'expo-av';
import {Permissions} from 'expo-permissions'
import styles from './stylesCamera';
import Toolbar from './toolbar.component';
import * as firebase from 'firebase';
import { Ionicons } from '@expo/vector-icons';

import BottomUpPanel from  './upSlider'


export default class CameraScreen extends React.Component {
    camera = null;

    state = {
        hasCameraPermission: null,
    };
    



    camera = null;
    state = {
        captures: [],
        // setting flash to be turned off by default
        flashMode: Camera.Constants.FlashMode.off,
        capturing: null,
        paused:false,
        // start the back camera by default
        cameraType: Camera.Constants.Type.back,
        hasCameraPermission: null,
        uid : null,
        msg:'Get Discription',
    };


    async componentDidMount() {
      const camera = await Camera.requestPermissionsAsync();
      const audio = await Audio.requestPermissionsAsync();
      const hasCameraPermission = (camera.status === 'granted' );

      this.setState({ hasCameraPermission });

      AsyncStorage.getItem('userid').then((value) => {
        this.setState({uid:value});
      });

  };

    setFlashMode = (flashMode) => this.setState({ flashMode });
    setCameraType = (cameraType) => this.setState({ cameraType });
    handleCaptureIn = () => this.setState({ capturing: true });

    handleCaptureOut = () => {
        if (this.state.capturing)
            this.camera.stopRecording();
    };

    handleCancel= () =>{
      this.camera.resumePreview();
      this.setState({ paused:false});
    };

    renderBottomUpPanelContent = () =>
          <View style = {{flex:1,backgroundColor:'rgba(0,0,0,0.8)',height:500}}>
            <Text style = {{color:'#FFF',fontSize:25}}>{this.state.msg1}</Text>
          </View>
          
  renderBottomUpPanelIcon = () =>
        <Ionicons name={"ios-arrow-up"} style={{color:"white"}} size={30}/>

    handleShortCapture = async () => {
      
        const photoData = await this.camera.takePictureAsync();
        this.camera.pausePreview();
        this.setState({ capturing: false,paused:true, captures: [photoData, ...this.state.captures] });

        var n=photoData.uri.lastIndexOf('/');
        var res = photoData.uri.slice(n+1,);
        console.log(photoData);

        if (!photoData.cancelled) {
          this.uploadImage(photoData.uri, res)
            .then(() => {
              console.log("Uploaded");
              
              
              
      fetch('http://192.168.137.158:8000/app/addimage/?url='+encodeURIComponent('https://firebasestorage.googleapis.com/v0/b/virtualtouristguide-d393f.appspot.com/o/virtualTouristApp%2F'+res+'?alt=media')+'&uid='+this.state.uid, {
      method: 'GET',
      headers: new Headers({
        'Content-Type': 'application/x-www-form-urlencoded',
      }),
      // body:'url=https://firebasestorage.googleapis.com/v0/b/virtualtouristguide-d393f.appspot.com/o/virtualTouristApp%2F9'+res+'?alt=media&uid='+AsyncStorage.getItem('userid'),
    }).then((response) => response.json())
    .then((responseJson) => {

      console.log(responseJson);
      if(responseJson.msg ==="success"){
        this.setState({msg1:responseJson.object +"\n" +responseJson.desc})
        
      }

    });
    console.log("Uploaded2");




            

            });
            // .catch((error) => {
            //   console.log("Not Uploaded");
            // });
        }
        console.log('Done');
        
    };
    

    uploadImage = async (uri, imageName) => {
      const response = await fetch(uri);
      const blob = await response.blob();
  
      var ref = firebase.storage().ref().child("virtualTouristApp/" + imageName);
      return ref.put(blob);
      
    }

    handleLongCapture = async () => {
        const videoData = await this.camera.recordAsync();
        this.setState({ capturing: false, captures: [videoData, ...this.state.captures] });
    };

    render() {
      const { hasCameraPermission, flashMode, cameraType, capturing,paused } = this.state;

        if (hasCameraPermission === null) {
            return <View />;
        } else if (hasCameraPermission === false) {
            return <Text>Access to camera has been denied.</Text>;
        }

        return (
          
          <React.Fragment>
          <View>
                    <Camera
                        type={cameraType}
                        flashMode={flashMode}
                        style={styles.preview}
                        ref={camera => this.camera = camera}
                    />
                </View>

                <Toolbar 
                    paused={paused}
                    capturing={capturing}
                    flashMode={flashMode}
                    cameraType={cameraType}
                    setFlashMode={this.setFlashMode}
                    setCameraType={this.setCameraType}
                    onCaptureIn={this.handleCaptureIn}
                    onCaptureOut={this.handleCaptureOut}
                    onLongCapture={this.handleLongCapture}
                    onShortCapture={this.handleShortCapture}
                    onCancel = {this.handleCancel}
                />
              
                <BottomUpPanel
content={this.renderBottomUpPanelContent}
icon={this.renderBottomUpPanelIcon}
topEnd={500}
startHeight={80}
headerText={"Swipe Up"}
headerTextStyle={{color:"white", 
                 fontSize: 15}}
bottomUpSlideBtn={{display: 'flex',
                 alignSelf: 'flex-start',
                 backgroundColor: 'black',
                 alignItems: 'center',
                 borderTopColor: 'grey',
                 borderTopWidth: 5}}>
</BottomUpPanel>
                
      </React.Fragment>


        );
    };
};
