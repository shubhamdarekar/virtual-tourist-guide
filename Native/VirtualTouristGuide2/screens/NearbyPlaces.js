import React from 'react';
import MapView from 'react-native-maps';
import { StyleSheet, Text, View, Dimensions,Button } from 'react-native';
import { Component } from 'react';


export default class App extends Component {
  constructor(props) {
      super(props);
      this.state = {
        markers: [
        {
          title: 'hello',
          coordinates: {
            latitude: 3.148561,
            longitude: 101.652778
          }
        },
        {
          title: 'ghyjgh',
          coordinates: {
            latitude: 3.5,
            longitude: 119.5
          },  
        }
      ]
    };
  }

  render() {
    return (
      <View style={styles.container}>
        <MapView
          style={styles.mapStyle}
          showsUserLocation={true}
          followsUserLocation={true}
          showsMyLocationButton={true}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  mapStyle: {
    width: Dimensions.get('window').width,
    height: Dimensions.get('window').height,
  }
});