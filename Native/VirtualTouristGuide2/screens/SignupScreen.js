import * as WebBrowser from 'expo-web-browser';
import React from 'react';
// import SubmitButton from 'react-native-submit-button';
import {
  Platform,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  TextInput,
  Button,
  
} from 'react-native';

//import { MonoText } from '../components/StyledText';

export default function RegisterScreen() {
  return (
    <View >
      <ScrollView>
        
    

        <View style={styles.getStartedContainer}>


          <Text style={styles.getStartedText}>Lets get started</Text>
          <Text style={styles.inputText}> Mobile number</Text>
          <TextInput  
          placeholder="Enter Your Mobile Number"  
          underlineColorAndroid='transparent'  
          style={styles.TextInputStyle}   
          keyboardType={'numeric'} 
        />  
        <Text style={styles.inputText}> Password</Text>
          <TextInput  
          placeholder="Enter Your Password"  
          underlineColorAndroid='transparent'  
          style={styles.TextInputStyle}   
          //keyboardType={'numeric'} 
        />  
        <Text style={styles.inputText}> Confirm Password</Text>
          <TextInput  
          placeholder="Enter Your Password again"  
          underlineColorAndroid='transparent'  
          style={styles.TextInputStyle}   
          //keyboardType={'numeric'} 
        />  
        {/* <SubmitButton /> */}
        </View>
        </ScrollView>
    </View>
    )
        
}



const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#E5E5E5',
  },


  getStartedContainer: {
    
    marginHorizontal: 50,
  },
  getStartedText: {
    paddingTop: 50,
    fontSize: 20,  
    fontWeight: 'bold',
    color: 'rgba(96,100,109, 1)',
    lineHeight: 24,
    
  },
  inputText: {
      paddingTop: 50,
      fontWeight: 'bold',
  },
  TextInputStyle: {  
    textAlign: 'center',  
    height: 40,  
    borderRadius: 10,  
    borderWidth: 2,  
    borderColor: '#000',  
    marginBottom: 10  
},  
  tabBarInfoContainer: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
    ...Platform.select({
      ios: {
        shadowColor: 'black',
        shadowOffset: { width: 0, height: -3 },
        shadowOpacity: 0.1,
        shadowRadius: 3,
      },
      android: {
        elevation: 20,
      },
    }),
},

})